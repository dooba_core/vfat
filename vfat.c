/* Dooba SDK
 * FAT32 Filesystem Driver
 */

// External Includes
#include <string.h>

// Internal Includes
#include "mountpoint.h"
#include "obj.h"
#include "obj_io.h"
#include "handle.h"
#include "vfat.h"

// File System
struct vfs_fs vfat_fs;

// Initialize VFAT Driver
void vfat_init()
{
	// Initialize File System Interface
	vfs_register(&vfat_fs, vfat_mount, vfat_unmount, vfat_mkobj, vfat_rmobj, vfat_mvobj, vfat_open, vfat_close, vfat_truncate, vfat_read, vfat_write, vfat_readdir, VFAT);
}
