/* Dooba SDK
 * FAT32 Filesystem Driver
 */

// External Includes
#include <string.h>

// Internal Includes
#include "bios_parameter_block.h"
#include "mountpoint.h"

// BIOS Parameter Block Buffer
struct vfat_bios_parameter_block vfat_bpb;

// Mount
uint8_t vfat_mount(struct vfs_mountpoint *mp, struct storage *st)
{
	struct vfat_mountpoint_data *d;

	// Load up BIOS Parameter Block
	if(storage_read(st, VFAT_BIOS_PARAMETER_BLOCK_ADDRESS, &vfat_bpb, sizeof(struct vfat_bios_parameter_block)))		{ return 1; }

	// Acquire & Setup FS Data Structure
	d = (struct vfat_mountpoint_data *)(mp->data);
	d->sectors = vfat_bpb.sector_count_small ? vfat_bpb.sector_count_small : vfat_bpb.sector_count_large;
	d->sector_size = vfat_bpb.bytes_per_sector;
	d->fat_addr = (uint32_t)(vfat_bpb.reserved_sectors) * d->sector_size;
	d->fat_size = vfat_bpb.sectors_per_fat * d->sector_size;
	d->data_addr = d->fat_addr + (d->fat_size * vfat_bpb.fat_count);
	d->data_size = ((uint64_t)(d->sectors) * (uint64_t)(d->sector_size)) - d->data_addr;
	d->cluster_size = d->sector_size * vfat_bpb.sectors_per_cluster;
	d->cluster_count = d->data_size / d->cluster_size;
	d->root_cluster = vfat_bpb.root_directory_start;
	d->last_free_cluster = 1;

	// Write Meth (Validate FAT params)
	if(memcmp(vfat_bpb.fs_type, "FAT32", 5))																			{ return 1; }

	return 0;
}

// Unmount
void vfat_unmount(struct vfs_mountpoint *mp)
{
	// NoOp
}
