/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_MOUNT_H
#define	__VFAT_MOUNT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <storage/storage.h>
#include <vfs/vfs.h>

// Mountpoint Data Structure
struct vfat_mountpoint_data
{
	// Total Sector Count
	uint32_t sectors;

	// Sector Size (Bytes)
	uint16_t sector_size;

	// FAT Region
	uint64_t fat_addr;
	uint32_t fat_size;

	// Data Region
	uint64_t data_addr;
	uint64_t data_size;

	// Cluster Size (Bytes)
	uint32_t cluster_size;

	// Cluster Count (Total)
	uint32_t cluster_count;

	// Root Directory Cluster Chain
	uint32_t root_cluster;

	// Last Free Cluster
	uint32_t last_free_cluster;
};

// Define this structure as VFS Mountpoint Data
vfs_mountpoint_data_struct(vfat_mountpoint_data);

// Mount
extern uint8_t vfat_mount(struct vfs_mountpoint *mp, struct storage *st);

// Unmount
extern void vfat_unmount(struct vfs_mountpoint *mp);

#endif
