/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_RESOLVE_H
#define	__VFAT_RESOLVE_H

// External Includes
#include <stdint.h>
#include <vfs/vfs.h>

// Internal Includes
#include "dir.h"

// Resolve Object in Mountpoint
extern uint8_t vfat_resolve(struct vfs_mountpoint *mp, char *path, uint8_t path_len, uint32_t *cluster, uint32_t *parent_dir_cluster_chain, uint32_t *parent_dir_entry_idx, uint32_t *parent_dir_lfn_entry_idx, struct vfat_dir_entry *parent_dir_entry);

#endif
