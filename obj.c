/* Dooba SDK
 * FAT32 Filesystem Driver
 */

// External Includes
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "cluster.h"
#include "resolve.h"
#include "dir.h"
#include "obj.h"

// Create Object
uint8_t vfat_mkobj(struct vfs_mountpoint *mp, char *path, uint8_t path_len, uint8_t otype)
{
	uint8_t parent_len;
	uint8_t name_s;
	uint8_t name_len;
	uint32_t dir_clust;
	uint32_t cluster;
	uint32_t cur_clust;
	uint32_t cur_clust_idx;
	struct vfat_dir_entry ebuf;

	// Determine Parent
	if(vfs_path_split(path, path_len, &parent_len, &name_s, &name_len))										{ return 1; }

	// Resolve Parent Directory Cluster Chain
	if(vfat_resolve(mp, path, parent_len, &dir_clust, 0, 0, 0, 0))											{ return 1; }

	// Allocate Cluster
	if(vfat_cluster_alloc(mp, &cluster))																	{ return 1; }

	// Check Directory
	if(otype == VFS_OTYPE_DIR)
	{
		/* A little note...
		 *
		 * We setup three initial directory entries here.
		 * Apparently, a well-formed vfat directory should have
		 * the two dot entries ('.' and '..') at the beginning.
		 * The third and last entry is simply the end-marker.
		 *
		 * - eresse
		 */

		// Generic Init - Clear entry
		memset(&ebuf, 0, sizeof(struct vfat_dir_entry));
		memset(ebuf.name, ' ', VFAT_DIR_ENTRY_NAME_LEN);
		memset(ebuf.ext, ' ', VFAT_DIR_ENTRY_EXT_LEN);
		ebuf.attr = VFAT_DIR_ENTRY_ATTR_SUBDIR;

		// Initialize '.'
		cur_clust_idx = 0;
		cur_clust = cluster;
		ebuf.name[0] = '.';
		ebuf.cluster_hi = cluster >> 16;
		ebuf.cluster_lo = cluster & 0x0000ffff;
		if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, 0, &ebuf))										{ vfat_cluster_free(mp, cluster); return 1; }

		// Initialize '..'
		cur_clust_idx = 0;
		cur_clust = cluster;
		ebuf.name[1] = '.';
		ebuf.cluster_hi = dir_clust >> 16;
		ebuf.cluster_lo = dir_clust & 0x0000ffff;
		if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, 1, &ebuf))										{ vfat_cluster_free(mp, cluster); return 1; }

		cur_clust_idx = 0;
		cur_clust = cluster;
		memset(&ebuf, 0, sizeof(struct vfat_dir_entry));
		if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, 2, &ebuf))										{ vfat_cluster_free(mp, cluster); return 1; }
	}

	// Insert Entry into Parent Directory
	if(vfat_dir_insert(mp, dir_clust, &(path[name_s]), name_len, otype, cluster, 0))						{ vfat_cluster_free(mp, cluster); return 1; }

	return 0;
}

// Destroy Object
uint8_t vfat_rmobj(struct vfs_mountpoint *mp, char *path, uint8_t path_len)
{
	uint32_t dir_clust;
	uint32_t cluster;
	uint32_t idx;
	uint32_t lfn_idx;
	uint32_t entries;

	// Resolve Parent Directory Cluster Chain
	if(vfat_resolve(mp, path, path_len, &cluster, &dir_clust, &idx, &lfn_idx, 0))							{ return 1; }
	entries = ((lfn_idx < idx) ? (idx - lfn_idx) : 0) + 1;

	// Remove Entry from Directory
	if(vfat_dir_remove(mp, dir_clust, lfn_idx, entries))													{ return 1; }

	// Free Cluster Chain
	if(vfat_cluster_free_chain(mp, cluster))																{ return 1; }

	return 0;
}

// Move Object
uint8_t vfat_mvobj(struct vfs_mountpoint *mp, char *src, uint8_t src_len, char *dst, uint8_t dst_len)
{
	uint32_t src_dir_clust;
	uint32_t dst_dir_clust;
	uint32_t cluster;
	uint32_t idx;
	uint32_t lfn_idx;
	uint32_t entries;
	uint8_t dst_parent_len;
	uint8_t dst_name_s;
	uint8_t dst_name_len;
	uint8_t otype;
	struct vfat_dir_entry entry;

	// Resolve Directory Cluster Chains
	if(vfs_path_split(dst, dst_len, &dst_parent_len, &dst_name_s, &dst_name_len))								{ return 1; }
	if(vfat_resolve(mp, src, src_len, &cluster, &src_dir_clust, &idx, &lfn_idx, &entry))						{ return 1; }
	if(vfat_resolve(mp, dst, dst_parent_len, &dst_dir_clust, 0, 0, 0, 0))										{ return 1; }
	entries = ((lfn_idx < idx) ? (idx - lfn_idx) : 0) + 1;

	// Insert Destination Entry into Directory
	otype = ((entry.attr & VFAT_DIR_ENTRY_ATTR_SUBDIR) == 0) ? VFS_OTYPE_FILE : VFS_OTYPE_DIR;
	if(vfat_dir_insert(mp, dst_dir_clust, &(dst[dst_name_s]), dst_name_len, otype, cluster, entry.fsize))		{ return 1; }

	// Remove Source Entry from Directory
	return vfat_dir_remove(mp, src_dir_clust, lfn_idx, entries);
}
