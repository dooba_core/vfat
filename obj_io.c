/* Dooba SDK
 * FAT32 Filesystem Driver
 */

// External Includes
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "cluster.h"
#include "handle.h"
#include "dir.h"
#include "obj_io.h"

// Entry Buffer
struct vfat_dir_entry vfat_obj_io_entry_buf;

// Truncate
uint8_t vfat_truncate(struct vfs_mountpoint *mp, struct vfs_handle *h, uint64_t size)
{
	struct vfat_mountpoint_data *m;
	struct vfat_handle_data *d;
	uint32_t last_clust;
	uint32_t last_clust_idx;
	uint32_t n;
	uint32_t drop_start_clust;
	uint32_t cur_clust;
	uint32_t cur_clust_idx;

	// Acquire Mountpoint Data
	m = (struct vfat_mountpoint_data *)(mp->data);

	// Acquire Handle Data Structure
	d = (struct vfat_handle_data *)(h->data);

	// Determine last cluster index
	last_clust_idx = vfat_cluster_idx(m, size ? (size - 1) : size);

	// Acquire last cluster & next (start of drop)
	if(vfat_cluster_get(mp, d->cluster_chain, last_clust_idx, &last_clust))											{ return 1; }
	if(vfat_cluster_get(mp, last_clust, 1, &drop_start_clust))														{ drop_start_clust = 0; }

	// Mark last cluster as EOC
	n = VFAT_CLUSTER_EOC;
	if(vfat_cluster_write_val(mp, m, last_clust, &n))																{ return 1; }

	// Drop Everything after last cluster
	if(drop_start_clust)																							{ vfat_cluster_free_chain(mp, drop_start_clust); }

	// Update File Size
	cur_clust = d->parent_chain;
	cur_clust_idx = 0;
	if(vfat_dir_read(mp, &cur_clust, &cur_clust_idx, d->parent_entry, &vfat_obj_io_entry_buf))						{ return 1; }
	vfat_obj_io_entry_buf.fsize = size;
	if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, d->parent_entry, &vfat_obj_io_entry_buf))						{ return 1; }
	h->size = size;

	return 0;
}

// Read File
uint8_t vfat_read(struct vfs_mountpoint *mp, struct vfs_handle *h, void *data, uint16_t size, uint16_t *rd)
{
	struct vfat_mountpoint_data *m;
	struct vfat_handle_data *d;
	uint16_t s;
	uint32_t o;
	uint32_t ci;
	uint8_t *p;

	// Acquire Mountpoint & Handle Data Structure
	m = (struct vfat_mountpoint_data *)(mp->data);
	d = (struct vfat_handle_data *)(h->data);

	// Loop
	if(rd)																											{ *rd = 0; }
	p = data;
	while(size)
	{
		// Determine How much we can read from this cluster
		o = h->pos % m->cluster_size;
		s = size;
		if((o + s) > m->cluster_size)																				{ s = m->cluster_size - o; }

		// Get Cluster
		ci = vfat_cluster_idx(m, h->pos);
		if(ci != d->cur_clust_idx)
		{
			if(vfat_cluster_get(mp, d->cur_clust, ci - d->cur_clust_idx, &(d->cur_clust)))							{ d->cur_clust_idx = 0; d->cur_clust = d->cluster_chain; return 1; }
			d->cur_clust_idx = ci;
		}

		// Read Data
		if(vfat_cluster_read(mp, m, d->cur_clust, o, p, s))															{ return 1; }

		// Move
		p = &(p[s]);
		h->pos = h->pos + s;
		size = size - s;
		if(rd)																										{ *rd = *rd + s; }
	}

	return 0;
}

// Write File
uint8_t vfat_write(struct vfs_mountpoint *mp, struct vfs_handle *h, void *data, uint16_t size, uint16_t *wr)
{
	struct vfat_mountpoint_data *m;
	struct vfat_handle_data *d;
	uint16_t s;
	uint32_t o;
	uint32_t ci;
	uint8_t *p;
	uint32_t cur_clust;
	uint32_t cur_clust_idx;

	// Acquire Mountpoint & Handle Data Structure
	m = (struct vfat_mountpoint_data *)(mp->data);
	d = (struct vfat_handle_data *)(h->data);

	// Loop
	if(wr)																											{ *wr = 0; }
	p = data;
	while(size)
	{
		// Determine How much we can read from this cluster
		o = h->pos % m->cluster_size;
		s = size;
		if((o + s) > m->cluster_size)																				{ s = m->cluster_size - o; }

		// Determine Cluster Index
		ci = vfat_cluster_idx(m, h->pos);

		// Do we need to extend the Cluster Chain?
		if(ci > vfat_cluster_idx(m, (h->size ? (h->size - 1) : 0)))
		{
			// Extend Cluster Chain
			if(vfat_cluster_chain_extend(mp, d->cur_clust, 0))														{ return 1; }
		}

		// Do we already know the Cluster?
		if(ci != d->cur_clust_idx)
		{
			// Fetch Cluster
			if(vfat_cluster_get(mp, d->cur_clust, ci - d->cur_clust_idx, &(d->cur_clust)))							{ d->cur_clust_idx = 0; d->cur_clust = d->cluster_chain; return 1; }
			d->cur_clust_idx = ci;
		}

		// Write Data
		if(vfat_cluster_write(mp, m, d->cur_clust, o, p, s))														{ return 1; }

		// Move
		p = &(p[s]);
		h->pos = h->pos + s;
		size = size - s;
		if(wr)																										{ *wr = *wr + s; }

		// Did we grow the file?
		if(h->pos > h->size)
		{
			// Update File Size
			cur_clust = d->parent_chain;
			cur_clust_idx = 0;
			if(vfat_dir_read(mp, &cur_clust, &cur_clust_idx, d->parent_entry, &vfat_obj_io_entry_buf))				{ return 1; }
			vfat_obj_io_entry_buf.fsize = h->pos;
			if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, d->parent_entry, &vfat_obj_io_entry_buf))				{ return 1; }
			h->size = h->pos;
		}
	}

	return 0;
}

// Read Directory
uint8_t vfat_readdir(struct vfs_mountpoint *mp, struct vfs_handle *h, struct vfs_dirent *e)
{
	struct vfat_handle_data *d;

	// Acquire Handle Data Structure
	d = (struct vfat_handle_data *)(h->data);

	// Read Next Directory
	if(vfat_dir_read_next(mp, &(d->cur_clust), &(d->cur_clust_idx), &(d->cur_idx), 0, &vfat_obj_io_entry_buf))		{ return 1; }

	// Check End Of Directory
	if(vfat_obj_io_entry_buf.name[0] == VFAT_DIR_ENTRY_NONE)														{ e->otype = VFS_OTYPE_NONE; }
	else
	{
		// Set Dir Entry
		if(vfat_dir_lfn_size)																						{ memcpy(e->name, vfat_dir_lfn_buf, vfat_dir_lfn_size); e->name_len = vfat_dir_lfn_size; }
		else																										{ vfat_dir_entry_get_short_name(vfat_obj_io_entry_buf.name, e->name, &(e->name_len)); }
		e->otype = (vfat_obj_io_entry_buf.attr & VFAT_DIR_ENTRY_ATTR_SUBDIR) ? VFS_OTYPE_DIR : VFS_OTYPE_FILE;
		e->size = (e->otype == VFS_OTYPE_FILE) ? vfat_obj_io_entry_buf.fsize : 0;

		// Next
		d->cur_idx = d->cur_idx + 1;
	}

	return 0;
}
