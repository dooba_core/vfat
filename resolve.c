/* Dooba SDK
 * FAT32 Filesystem Driver
 */

// External Includes
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "mountpoint.h"
#include "resolve.h"

// Directory Entry Buffer
struct vfat_dir_entry vfat_resolve_ebuf;

// Resolve Object in Mountpoint
uint8_t vfat_resolve(struct vfs_mountpoint *mp, char *path, uint8_t path_len, uint32_t *cluster, uint32_t *parent_dir_cluster_chain, uint32_t *parent_dir_entry_idx, uint32_t *parent_dir_lfn_entry_idx, struct vfat_dir_entry *parent_dir_entry)
{
	struct vfat_mountpoint_data *d;
	uint32_t cur_dir_clust;
	uint32_t e_idx;
	uint32_t e_lfn_idx;
	uint8_t sub_p;
	uint8_t sub_p_len;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Check Root
	if(path_len == 0)																									{ return 1; }
	if(path[0] != VFS_PATH_DELIM)																						{ return 1; }

	// Run through Path starting at root
	sub_p = 0;
	sub_p_len = 0;
	cur_dir_clust = d->root_cluster;
	memset(&vfat_resolve_ebuf, 0, sizeof(struct vfat_dir_entry));
	vfat_resolve_ebuf.attr = VFAT_DIR_ENTRY_ATTR_SUBDIR;
	vfat_resolve_ebuf.cluster_hi = cur_dir_clust >> 16;
	vfat_resolve_ebuf.cluster_lo = cur_dir_clust & 0x0000ffff;
	while(sub_p < path_len)
	{
		// Acquire Part
		sub_p = sub_p + 1;
		if(path_len - sub_p)
		{
			// Get Part Length
			sub_p_len = str_chr(&(path[sub_p]), path_len - sub_p, VFS_PATH_DELIM);
			if(sub_p_len > VFS_PATH_ELEMENT_MAXLEN)																		{ return 1; }

			// Find in Directory
			e_idx = 0;
			e_lfn_idx = 0;
			if(vfat_dir_find(mp, cur_dir_clust, &(path[sub_p]), sub_p_len, &e_idx, &e_lfn_idx, &vfat_resolve_ebuf))		{ return 1; }

			// Check Intermediate Part
			if((sub_p + sub_p_len) != path_len)
			{
				// Verify it's a Directory
				if((vfat_resolve_ebuf.attr & VFAT_DIR_ENTRY_ATTR_SUBDIR) == 0)											{ return 1; }

				// Enter Next Directory
				cur_dir_clust = ((uint32_t)(vfat_resolve_ebuf.cluster_hi) << 16) + vfat_resolve_ebuf.cluster_lo;
			}

			// Move
			sub_p = sub_p + sub_p_len;
		}
	}

	// Found
	if(cluster)																											{ *cluster = ((uint32_t)(vfat_resolve_ebuf.cluster_hi) << 16) + vfat_resolve_ebuf.cluster_lo; }
	if(parent_dir_cluster_chain)																						{ *parent_dir_cluster_chain = cur_dir_clust; }
	if(parent_dir_entry_idx)																							{ *parent_dir_entry_idx = e_idx; }
	if(parent_dir_lfn_entry_idx)																						{ *parent_dir_lfn_entry_idx = e_lfn_idx; }
	if(parent_dir_entry)																								{ memcpy(parent_dir_entry, &vfat_resolve_ebuf, sizeof(struct vfat_dir_entry)); }

	return 0;
}
