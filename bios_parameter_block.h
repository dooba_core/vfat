/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_BIOS_PARAMETER_BLOCK_H
#define	__VFAT_BIOS_PARAMETER_BLOCK_H

// External Includes
#include <stdint.h>
#include <mbr/vbr.h>

// Address of Boot Parameter Block
#define	VFAT_BIOS_PARAMETER_BLOCK_ADDRESS				(sizeof(struct mbr_vbr))

// Volume Label Length
#define	VFAT_BIOS_PARAMETER_BLOCK_VOLUME_LABEL_LEN		11

// FS Type Length
#define	VFAT_BIOS_PARAMETER_BLOCK_FS_TYPE_LEN			8

// BIOS Parameter Block Structure
struct vfat_bios_parameter_block
{
	// Bytes per Sector
	uint16_t bytes_per_sector;

	// Sectors per Cluster
	uint8_t sectors_per_cluster;

	// Reserved Sectors
	uint16_t reserved_sectors;

	// Number of FATs
	uint8_t fat_count;

	// (Obsolete) Maximum number of Root Directory Entries
	uint16_t max_root_entries;

	// Sector Count (Small Version - if 0, use Large Version)
	uint16_t sector_count_small;

	// Media Descriptor
	uint8_t media_descriptor;

	// (Obsolete) Sectors per FAT
	uint16_t sectors_per_fat_old;

	// Sectors per Track
	uint16_t sectors_per_track;

	// Number of Heads
	uint16_t head_count;

	// Hidden Sectors
	uint32_t hidden_sectors;

	// Sector Count (Large Version)
	uint32_t sector_count_large;

	// Sectors per FAT
	uint32_t sectors_per_fat;

	// Mirroring Flags
	uint16_t mirroring_flags;

	// Version
	uint16_t version;

	// Start of Root Directory Cluster Chain
	uint32_t root_directory_start;

	// FS Information Sector
	uint16_t fs_info_sector;

	// Start of Boot Area Copy Sectors
	uint16_t boot_area_copy_start;

	// Reserved Data
	uint8_t reserved_0[12];

	// Physical Drive Number
	uint8_t physical_drive_number;

	// Reserved
	uint8_t reserved_1;

	// Extended Boot Signature
	uint8_t extended_boot_signature;

	// Volume ID
	uint32_t volume_id;

	// Volume Label
	char volume_label[VFAT_BIOS_PARAMETER_BLOCK_VOLUME_LABEL_LEN];

	// FS Type
	char fs_type[VFAT_BIOS_PARAMETER_BLOCK_FS_TYPE_LEN];
};

#endif
