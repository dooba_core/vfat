/* Dooba SDK
 * FAT32 Filesystem Driver
 */

// External Includes
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "cluster.h"
#include "resolve.h"
#include "dir.h"
#include "handle.h"

// Directory Entry Buffer
struct vfat_dir_entry vfat_handle_dir_entry_buf;

// Open Handle on Object
uint8_t vfat_open(struct vfs_mountpoint *mp, struct vfs_handle *h, char *path, uint8_t path_len)
{
	struct vfat_handle_data *d;

	// Acquire Handle Data Structure
	d = (struct vfat_handle_data *)(h->data);

	// Resolve Path including Parent Directory Cluster Chain & Entry
	if(vfat_resolve(mp, path, path_len, &(d->cluster_chain), &(d->parent_chain), &(d->parent_entry), 0, &vfat_handle_dir_entry_buf))	{ return 1; }

	// Complete Handle Information
	h->otype = ((vfat_handle_dir_entry_buf.attr & VFAT_DIR_ENTRY_ATTR_SUBDIR) == 0) ? VFS_OTYPE_FILE : VFS_OTYPE_DIR;
	h->size = vfat_handle_dir_entry_buf.fsize;

	// Clear Current Cluster
	d->cur_clust_idx = 0;
	d->cur_clust = d->cluster_chain;

	// Reset Current Directory Entry
	d->cur_idx = 0;

	return 0;
}

// Close Handle
void vfat_close(struct vfs_mountpoint *mp, struct vfs_handle *h)
{
	// NoOp
}

// Sync Current Cluster to Position
void vfat_handle_sync_cur_clust(struct vfs_mountpoint *mp, struct vfs_handle *h)
{
	struct vfat_mountpoint_data *mpd;
	struct vfat_handle_data *d;

	// Acquire Mountpoint Data
	mpd = (struct vfat_mountpoint_data *)(mp->data);

	// Acquire Handle Data Structure
	d = (struct vfat_handle_data *)(h->data);

	// Check if we went backwards
	if(vfat_cluster_idx(mpd, h->pos) < d->cur_clust_idx)
	{
		// Reset Current Cluster to start of Chain
		d->cur_clust_idx = 0;
		d->cur_clust = d->cluster_chain;
	}
}
