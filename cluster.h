/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_CLUSTER_H
#define	__VFAT_CLUSTER_H

// External Includes
#include <stdint.h>
#include <storage/storage.h>
#include <vfs/vfs.h>

// Internal Includes
#include "mountpoint.h"

// EOC Marker (End Of Chain)
#define	VFAT_CLUSTER_EOC																0x0fffffff
#define	VFAT_CLUSTER_EOC_MASK															0x0ffffff8

// Free Cluster / EOC
#define	VFAT_CLUSTER_FREE_MASK															0xf0000000
#define	VFAT_CLUSTER_FREE																0x00000000

// Buffer Size
#define	VFAT_CLUSTER_BUFSIZE															32

// Get Cluster Index for Byte Offset
#define	vfat_cluster_idx(mp_data, off)													((off) / (mp_data)->cluster_size)

// Cluster Data Address
#define	vfat_cluster_data(mp_data, cluster)												((mp_data)->data_addr + ((((uint64_t)cluster) - 2) * (mp_data)->cluster_size))

// Cluster Address in FAT
#define	vfat_cluster_addr_fat(mp_data, cluster)											((mp_data)->fat_addr + (((uint64_t)cluster) * sizeof(uint32_t)))

// Cluster FAT I/O
#define	vfat_cluster_read_val(mp, mp_data, cluster, v)									storage_read((mp)->st, vfat_cluster_addr_fat(mp_data, cluster), v, sizeof(uint32_t))
#define	vfat_cluster_write_val(mp, mp_data, cluster, v)									storage_write((mp)->st, vfat_cluster_addr_fat(mp_data, cluster), v, sizeof(uint32_t))

// Cluster Data I/O
#define	vfat_cluster_read(mp, mp_data, cluster, offset, data, size)						storage_read((mp)->st, vfat_cluster_data(mp_data, cluster) + (offset), data, size)
#define	vfat_cluster_write(mp, mp_data, cluster, offset, data, size)					storage_write((mp)->st, vfat_cluster_data(mp_data, cluster) + (offset), data, size)

// Is Cluster Free?
#define	vfat_cluster_is_free(n)															((n | VFAT_CLUSTER_FREE_MASK) == VFAT_CLUSTER_FREE_MASK)

// Is Cluster EOC?
#define	vfat_cluster_is_eoc(n)															(vfat_cluster_is_free(n) || ((n & VFAT_CLUSTER_EOC_MASK) == VFAT_CLUSTER_EOC_MASK))

// Get Cluster from Chain
extern uint8_t vfat_cluster_get(struct vfs_mountpoint *mp, uint32_t chain_start, uint32_t idx, uint32_t *cluster);

// Allocate Cluster
extern uint8_t vfat_cluster_alloc(struct vfs_mountpoint *mp, uint32_t *cluster);

// Free Cluster
extern uint8_t vfat_cluster_free(struct vfs_mountpoint *mp, uint32_t cluster);

// Free Cluster Chain
extern uint8_t vfat_cluster_free_chain(struct vfs_mountpoint *mp, uint32_t cluster);

// Extend Cluster Chain (allocate cluster)
extern uint8_t vfat_cluster_chain_extend(struct vfs_mountpoint *mp, uint32_t chain_start, uint32_t *cluster);

// Reduce Cluster Chain (free cluster)
extern uint8_t vfat_cluster_chain_reduce(struct vfs_mountpoint *mp, uint32_t chain_start);

#endif
