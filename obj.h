/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_OBJ_H
#define	__VFAT_OBJ_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <storage/storage.h>
#include <vfs/vfs.h>

// Create Object
extern uint8_t vfat_mkobj(struct vfs_mountpoint *mp, char *path, uint8_t path_len, uint8_t otype);

// Destroy Object
extern uint8_t vfat_rmobj(struct vfs_mountpoint *mp, char *path, uint8_t path_len);

// Move Object
extern uint8_t vfat_mvobj(struct vfs_mountpoint *mp, char *src, uint8_t src_len, char *dst, uint8_t dst_len);

#endif
