/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_HANDLE_H
#define	__VFAT_HANDLE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <storage/storage.h>
#include <vfs/vfs.h>

// Handle Data Structure
struct vfat_handle_data
{
	// Parent Directory Chain
	uint32_t parent_chain;

	// Parent Directory Entry Index
	uint32_t parent_entry;

	// Cluster Chain
	uint32_t cluster_chain;

	// Current Cluster
	uint32_t cur_clust_idx;
	uint32_t cur_clust;

	// Current Directory Entry
	uint32_t cur_idx;
};

// Define this structure as VFS Handle Data
vfs_handle_data_struct(vfat_handle_data);

// Open Handle on Object
extern uint8_t vfat_open(struct vfs_mountpoint *mp, struct vfs_handle *h, char *path, uint8_t path_len);

// Close Handle
extern void vfat_close(struct vfs_mountpoint *mp, struct vfs_handle *h);

// Sync Current Cluster to Position
extern void vfat_handle_sync_cur_clust(struct vfs_mountpoint *mp, struct vfs_handle *h);

#endif
