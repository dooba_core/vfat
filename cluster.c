/* Dooba SDK
 * FAT32 Filesystem Driver
 */

// External Includes
#include <string.h>

// Internal Includes
#include "cluster.h"

// Cluster Data Buffer
uint8_t vfat_cluster_buf[VFAT_CLUSTER_BUFSIZE];

// Get Cluster from Chain
uint8_t vfat_cluster_get(struct vfs_mountpoint *mp, uint32_t chain_start, uint32_t idx, uint32_t *cluster)
{
	struct vfat_mountpoint_data *d;
	uint32_t n;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Run through Cluster Chain
	n = chain_start;
	*cluster = n;
	while(idx > 0)
	{
		// Read Cluster Value in FAT
		if(vfat_cluster_read_val(mp, d, n, cluster))											{ return 1; }

		// Detect End Of Chain
		n = *cluster;
		if(vfat_cluster_is_eoc(n))																{ return 1; }

		// Next
		idx = idx - 1;
	}

	// Found
	return 0;
}

// Allocate Cluster
uint8_t vfat_cluster_alloc(struct vfs_mountpoint *mp, uint32_t *cluster)
{
	struct vfat_mountpoint_data *d;
	uint32_t n;
	uint32_t i;
	uint32_t j;
	uint8_t s;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Find Free Cluster
	i = d->last_free_cluster + 1;
	while(i != d->last_free_cluster)
	{
		// Read Value & Check Free
		if(vfat_cluster_read_val(mp, d, i, &n))													{ return 1; }
		if(vfat_cluster_is_free(n))
		{
			// Allocate & Clear Cluster
			j = 0;
			while(j < d->cluster_size)
			{
				s = ((d->cluster_size - j) < VFAT_CLUSTER_BUFSIZE) ? (d->cluster_size - j) : VFAT_CLUSTER_BUFSIZE;
				memset(vfat_cluster_buf, 0, s);
				if(vfat_cluster_write(mp, d, i, j, vfat_cluster_buf, s))						{ return 1; }
				j = j + s;
			}
			*cluster = i;
			n = VFAT_CLUSTER_EOC;
			d->last_free_cluster = i;
			return vfat_cluster_write_val(mp, d, i, &n);
		}

		// Cycle
		i = i + 1;
		if(i >= d->cluster_count)																{ i = 1; }
	}

	return 1;
}

// Free Cluster
uint8_t vfat_cluster_free(struct vfs_mountpoint *mp, uint32_t cluster)
{
	struct vfat_mountpoint_data *d;
	uint32_t n;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Free
	n = VFAT_CLUSTER_FREE;
	return vfat_cluster_write_val(mp, d, cluster, &n);
}

// Free Cluster Chain
uint8_t vfat_cluster_free_chain(struct vfs_mountpoint *mp, uint32_t cluster)
{
	struct vfat_mountpoint_data *d;
	uint32_t n;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Loop
	while(!vfat_cluster_is_eoc(cluster))
	{
		// Read Cluster Value
		if(vfat_cluster_read_val(mp, d, cluster, &n))											{ return 1; }

		// Free Cluster
		if(vfat_cluster_free(mp, cluster))														{ return 1; }

		// Next
		cluster = n;
	}

	return 0;
}

// Extend Cluster Chain (allocate cluster)
uint8_t vfat_cluster_chain_extend(struct vfs_mountpoint *mp, uint32_t chain_start, uint32_t *cluster)
{
	struct vfat_mountpoint_data *d;
	uint32_t fc;
	uint32_t n;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Allocate Cluster
	if(vfat_cluster_alloc(mp, &fc))																{ return 1; }

	// Add Cluster to Chain
	while(vfat_cluster_get(mp, chain_start, 1, &n) == 0)										{ chain_start = n; }
	if(vfat_cluster_write_val(mp, d, chain_start, &fc))											{ return 1; }

	// Set Cluster
	if(cluster)																					{ *cluster = fc; }

	return 0;
}

// Reduce Cluster Chain (free cluster)
uint8_t vfat_cluster_chain_reduce(struct vfs_mountpoint *mp, uint32_t chain_start)
{
	struct vfat_mountpoint_data *d;
	uint32_t n;
	uint32_t prev;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Find last clusters
	prev = 0;
	while(vfat_cluster_get(mp, chain_start, 1, &n) == 0)
	{
		prev = chain_start;
		chain_start = n;
	}

	// Reduce
	n = VFAT_CLUSTER_EOC;
	if(vfat_cluster_write_val(mp, d, prev, &n))													{ return 1; }
	return vfat_cluster_free(mp, chain_start);
}
