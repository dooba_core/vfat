/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_H
#define	__VFAT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <storage/storage.h>
#include <vfs/vfs.h>

// VFAT Driver Name
#define	VFAT							"vfat"

// Initialize VFAT Driver
extern void vfat_init();

#endif
