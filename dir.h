/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_DIR_H
#define	__VFAT_DIR_H

// External Includes
#include <stdint.h>
#include <vfs/vfs.h>

// Name Length
#define	VFAT_DIR_ENTRY_NAME_LEN							8

// Extension Length
#define	VFAT_DIR_ENTRY_EXT_LEN							3

// Short Name Length (Total)
#define	VFAT_DIR_ENTRY_SNAME_LEN						(VFAT_DIR_ENTRY_NAME_LEN + VFAT_DIR_ENTRY_EXT_LEN)

// Attributes
#define	VFAT_DIR_ENTRY_ATTR_READONLY					0x01
#define	VFAT_DIR_ENTRY_ATTR_HIDDEN						0x02
#define	VFAT_DIR_ENTRY_ATTR_SYSTEM						0x04
#define	VFAT_DIR_ENTRY_ATTR_VOLLABEL					0x08
#define	VFAT_DIR_ENTRY_ATTR_SUBDIR						0x10
#define	VFAT_DIR_ENTRY_ATTR_ARCHIVE						0x20
#define	VFAT_DIR_ENTRY_ATTR_DEVICE						0x40
#define	VFAT_DIR_ENTRY_ATTR_RESERVED					0x80
#define	VFAT_DIR_ENTRY_ATTR_LFN							(VFAT_DIR_ENTRY_ATTR_READONLY | VFAT_DIR_ENTRY_ATTR_HIDDEN | VFAT_DIR_ENTRY_ATTR_SYSTEM | VFAT_DIR_ENTRY_ATTR_VOLLABEL)

// LFN Entry Name Lengths (UCS-2 Characters)
#define	VFAT_DIR_ENTRY_LFN_NAME_LEN_1					5
#define	VFAT_DIR_ENTRY_LFN_NAME_LEN_2					6
#define	VFAT_DIR_ENTRY_LFN_NAME_LEN_3					2
#define	VFAT_DIR_ENTRY_LFN_NAME_LEN_TOTAL				(VFAT_DIR_ENTRY_LFN_NAME_LEN_1 + VFAT_DIR_ENTRY_LFN_NAME_LEN_2 + VFAT_DIR_ENTRY_LFN_NAME_LEN_3)

// Special Markers
#define	VFAT_DIR_ENTRY_NONE								0x00
#define	VFAT_DIR_ENTRY_AVAIL							0xe5

// LFN Sequence Number
#define	VFAT_DIR_ENTRY_LFN_SEQNUM_FIRSTLAST_FLAG		0x40
#define	VFAT_DIR_ENTRY_LFN_SEQNUM_MASK					0x0f

// Maximum Short Name Index
#define	VFAT_DIR_ENTRY_SNAME_IDX_MAX					10000

// Byte Offset for Directory Entry
#define	vfat_dir_entry_offset(idx)						((uint64_t)(idx) * sizeof(struct vfat_dir_entry))

// Cluster Index (within Directory Chain) for Directory Entry
#define	vfat_dir_entry_cluster_idx(mp_data, idx)		(vfat_dir_entry_offset(idx) / (mp_data)->cluster_size)

// Directory Entry Structure
struct vfat_dir_entry
{
	// Name
	uint8_t name[VFAT_DIR_ENTRY_NAME_LEN];

	// Extension
	uint8_t ext[VFAT_DIR_ENTRY_EXT_LEN];

	// Attributes
	uint8_t attr;

	// Zero
	uint16_t zero;

	// Creation Time & Date
	uint16_t creation_time;
	uint16_t creation_date;

	// Last Access Date
	uint16_t last_access_date;

	// Cluster Chain High Bytes
	uint16_t cluster_hi;

	// Last Modified Time & Date
	uint16_t last_modified_time;
	uint16_t last_modified_date;

	// Cluster Chain Low Bytes
	uint16_t cluster_lo;

	// File Size
	uint32_t fsize;
};

// LFN Directory Entry Structure
struct vfat_dir_entry_lfn
{
	// Sequence Number
	uint8_t seqnum;

	// Name as UCS-2 (part 1)
	uint16_t name_1[VFAT_DIR_ENTRY_LFN_NAME_LEN_1];

	// Attributes (always 0x0f for LFN)
	uint8_t attr;

	// Zero
	uint8_t zero;

	// Checksum
	uint8_t csum;

	// Name as UCS-2 (part 2)
	uint16_t name_2[VFAT_DIR_ENTRY_LFN_NAME_LEN_2];

	// First Cluster (always 0)
	uint16_t cluster;

	// Name as UCS-2 (part 3)
	uint16_t name_3[VFAT_DIR_ENTRY_LFN_NAME_LEN_3];
};

// LFN Buffer
extern char vfat_dir_lfn_buf[VFS_PATH_ELEMENT_MAXLEN];
extern uint8_t vfat_dir_lfn_size;

// Find Entry in Directory
extern uint8_t vfat_dir_find(struct vfs_mountpoint *mp, uint32_t chain_start, char *name, uint8_t name_len, uint32_t *entry_idx, uint32_t *entry_lfn_idx, struct vfat_dir_entry *e);

// Read Next Entry in Directory
extern uint8_t vfat_dir_read_next(struct vfs_mountpoint *mp, uint32_t *current_cluster, uint32_t *current_cluster_idx, uint32_t *idx, uint32_t *lfn_idx, struct vfat_dir_entry *e);

// Insert Entry into Directory
extern uint8_t vfat_dir_insert(struct vfs_mountpoint *mp, uint32_t chain_start, char *name, uint8_t name_len, uint8_t otype, uint32_t cluster, uint32_t size);

// Remove Entries from Directory
extern uint8_t vfat_dir_remove(struct vfs_mountpoint *mp, uint32_t chain_start, uint32_t entry_idx, uint32_t entries);

// Read Directory Entry
extern uint8_t vfat_dir_read(struct vfs_mountpoint *mp, uint32_t *current_cluster, uint32_t *current_cluster_idx, uint32_t entry_idx, struct vfat_dir_entry *e);

// Write Directory Entry
extern uint8_t vfat_dir_write(struct vfs_mountpoint *mp, uint32_t *current_cluster, uint32_t *current_cluster_idx, uint32_t entry_idx, struct vfat_dir_entry *e);

// Allocate Directory Entries
extern uint8_t vfat_dir_alloc_entries(struct vfs_mountpoint *mp, uint32_t chain_start, uint8_t entries, uint32_t *idx);

// Allocate Short Name for Object
extern uint8_t vfat_dir_alloc_sname(struct vfs_mountpoint *mp, uint32_t chain_start, char *name, uint8_t name_len, uint8_t *short_name);

// Compute Checksum for Short Name
extern uint8_t vfat_dir_short_name_csum(uint8_t *short_name);

// Extract Short Name from Directory Entry
extern void vfat_dir_entry_get_short_name(uint8_t *short_name, char *buf, uint8_t *len);

#endif
