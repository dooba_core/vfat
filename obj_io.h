/* Dooba SDK
 * FAT32 Filesystem Driver
 */

#ifndef	__VFAT_OBJ_IO_H
#define	__VFAT_OBJ_IO_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <storage/storage.h>
#include <vfs/vfs.h>

// Truncate
extern uint8_t vfat_truncate(struct vfs_mountpoint *mp, struct vfs_handle *h, uint64_t size);

// Read File
extern uint8_t vfat_read(struct vfs_mountpoint *mp, struct vfs_handle *h, void *data, uint16_t size, uint16_t *rd);

// Write File
extern uint8_t vfat_write(struct vfs_mountpoint *mp, struct vfs_handle *h, void *data, uint16_t size, uint16_t *wr);

// Read Directory
extern uint8_t vfat_readdir(struct vfs_mountpoint *mp, struct vfs_handle *h, struct vfs_dirent *e);

#endif
