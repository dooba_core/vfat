/* Dooba SDK
 * FAT32 Filesystem Driver
 */

// External Includes
#include <string.h>
#include <util/str.h>
#include <util/scase.h>
#include <util/cprintf.h>

// Internal Includes
#include "cluster.h"
#include "mountpoint.h"
#include "resolve.h"
#include "dir.h"

// Entry Buffer
struct vfat_dir_entry vfat_dir_entry_buf;

// Index Buffer
uint8_t vfat_dir_sname_idx_buf[VFAT_DIR_ENTRY_NAME_LEN];

// LFN Buffer
char vfat_dir_lfn_buf_p[VFAT_DIR_ENTRY_LFN_NAME_LEN_TOTAL];
char vfat_dir_lfn_buf[VFS_PATH_ELEMENT_MAXLEN];
uint8_t vfat_dir_lfn_size;

// Find Entry in Directory
uint8_t vfat_dir_find(struct vfs_mountpoint *mp, uint32_t chain_start, char *name, uint8_t name_len, uint32_t *entry_idx, uint32_t *entry_lfn_idx, struct vfat_dir_entry *e)
{
	uint8_t done;
	uint32_t cidx;
	uint32_t clust;
	uint8_t lfn_size_p;

	// Run through Directory Entries
	done = 0;
	cidx = 0;
	clust = chain_start;
	while(!done)
	{
		// Read Next
		if(vfat_dir_read_next(mp, &clust, &cidx, entry_idx, entry_lfn_idx, e))							{ return 1; }

		// Check End Of Directory
		if(e->name[0] == VFAT_DIR_ENTRY_NONE)															{ return 1; }

		// Check LFN
		if(str_cmp(vfat_dir_lfn_buf, vfat_dir_lfn_size, name, name_len) == 0)							{ return 0; }

		// LFN Doesn't match but maybe Short Name does - Extract & Check Short Name
		vfat_dir_entry_get_short_name(e->name, vfat_dir_lfn_buf_p, &lfn_size_p);
		if(str_ccmp(vfat_dir_lfn_buf_p, lfn_size_p, name, name_len) == 0)								{ return 0; }

		// Next
		*entry_idx = *entry_idx + 1;
	}

	return 1;
}

// Read Next Entry in Directory
uint8_t vfat_dir_read_next(struct vfs_mountpoint *mp, uint32_t *current_cluster, uint32_t *current_cluster_idx, uint32_t *idx, uint32_t *lfn_idx, struct vfat_dir_entry *e)
{
	uint8_t done;
	uint32_t eidx;
	uint8_t lfn_csum;
	uint8_t lfn_size_p;
	uint32_t first_lfn_entry;
	struct vfat_dir_entry_lfn *lfn;
	uint8_t i;

	// Run through Directory Entries
	done = 0;
	eidx = *idx;
	lfn_csum = 0;
	lfn_size_p = 0;
	vfat_dir_lfn_size = 0;
	first_lfn_entry = 0;
	while(!done)
	{
		// Read Next Entry in Directory
		if(vfat_dir_read(mp, current_cluster, current_cluster_idx, eidx, e))							{ return 1; }

		// Check End Of Directory
		if(e->name[0] == VFAT_DIR_ENTRY_NONE)
		{
			// Done
			*idx = eidx;
			if(lfn_idx)																					{ *lfn_idx = eidx; }
			return 0;
		}

		// Check Available / Deleted
		if(e->name[0] == VFAT_DIR_ENTRY_AVAIL)															{ vfat_dir_lfn_size = 0; }
		else if((e->name[0] == '.') && (e->name[1] == ' '))												{ vfat_dir_lfn_size = 0; }
		else if((e->name[0] == '.') && (e->name[1] == '.') && (e->name[2] == ' '))						{ vfat_dir_lfn_size = 0; }
		else
		{
			// Check LFN Entry
			if((e->attr & VFAT_DIR_ENTRY_ATTR_LFN) == VFAT_DIR_ENTRY_ATTR_LFN)
			{
				// LFN Entry - Acquire LFN Entry Pointer & Extract Name
				lfn = (struct vfat_dir_entry_lfn *)e;
				for(i = 0; i < VFAT_DIR_ENTRY_LFN_NAME_LEN_1; i = i + 1)								{ vfat_dir_lfn_buf_p[i] = lfn->name_1[i] & 0x00ff; }
				for(i = 0; i < VFAT_DIR_ENTRY_LFN_NAME_LEN_2; i = i + 1)								{ vfat_dir_lfn_buf_p[VFAT_DIR_ENTRY_LFN_NAME_LEN_1 + i] = lfn->name_2[i] & 0x00ff; }
				for(i = 0; i < VFAT_DIR_ENTRY_LFN_NAME_LEN_3; i = i + 1)								{ vfat_dir_lfn_buf_p[VFAT_DIR_ENTRY_LFN_NAME_LEN_1 + VFAT_DIR_ENTRY_LFN_NAME_LEN_2 + i] = lfn->name_3[i] & 0x00ff; }
				lfn_size_p = VFAT_DIR_ENTRY_LFN_NAME_LEN_TOTAL;
				for(i = lfn_size_p - 1; i > 0; i = i - 1)												{ if(vfat_dir_lfn_buf_p[i] == 0) { lfn_size_p = i; } }

				// Check First Physical / Last Logical Entry & Checksum
				if((lfn->seqnum & VFAT_DIR_ENTRY_LFN_SEQNUM_FIRSTLAST_FLAG) == 0)						{ if(lfn_csum != lfn->csum) { vfat_dir_lfn_size = 0; } }
				else
				{
					// First Physical LFN Entry (Last Logical) - Register & Set Size
					lfn_csum = lfn->csum;
					first_lfn_entry = eidx;
					vfat_dir_lfn_size = (((lfn->seqnum & VFAT_DIR_ENTRY_LFN_SEQNUM_MASK) - 1) * VFAT_DIR_ENTRY_LFN_NAME_LEN_TOTAL) + lfn_size_p;
					if(vfat_dir_lfn_size > VFS_PATH_ELEMENT_MAXLEN)										{ vfat_dir_lfn_size = 0; }
				}

				// Only Construct LFN buffer if the size is acceptable
				if(vfat_dir_lfn_size)																	{ memcpy(&(vfat_dir_lfn_buf[((lfn->seqnum & VFAT_DIR_ENTRY_LFN_SEQNUM_MASK) - 1) * VFAT_DIR_ENTRY_LFN_NAME_LEN_TOTAL]), vfat_dir_lfn_buf_p, lfn_size_p); }
			}
			else
			{
				// Compute Checksum for Short Name (Invalidate LFN if Checksum doesn't match)
				i = vfat_dir_short_name_csum(e->name);
				if(i != lfn_csum)																		{ vfat_dir_lfn_size = 0; }

				// We actually found something here - return it
				done = 1;
			}
		}

		// Next Directory Entry
		if(!done)																						{ eidx = eidx + 1; }
	}

	// Set Results
	*idx = eidx;
	if(lfn_idx)																							{ *lfn_idx = (vfat_dir_lfn_size > 0) ? first_lfn_entry : eidx; }

	return 0;
}

// Insert Entry into Directory
uint8_t vfat_dir_insert(struct vfs_mountpoint *mp, uint32_t chain_start, char *name, uint8_t name_len, uint8_t otype, uint32_t cluster, uint32_t size)
{
	struct vfat_dir_entry_lfn *lfn;
	uint8_t sname[VFAT_DIR_ENTRY_SNAME_LEN];
	uint32_t cur_clust_idx;
	uint32_t cur_clust;
	uint8_t lfn_chunks;
	uint8_t lfn_idx;
	uint8_t csum;
	uint32_t idx;
	uint8_t i;
	uint8_t j;
	uint8_t k;

	// Determine number of LFN chunks required
	lfn_chunks = (name_len / VFAT_DIR_ENTRY_LFN_NAME_LEN_TOTAL) + ((name_len % VFAT_DIR_ENTRY_LFN_NAME_LEN_TOTAL) ? 1 : 0);

	// Allocate Short Name
	if(vfat_dir_alloc_sname(mp, chain_start, name, name_len, sname))									{ return 1; }

	// Allocate Entries for LFN chunks and actual object
	if(vfat_dir_alloc_entries(mp, chain_start, lfn_chunks + 1, &idx))									{ return 1; }

	// Compute Checksum for Allocated Short Name
	csum = vfat_dir_short_name_csum(sname);

	// Setup LFN chunks
	k = 0;
	lfn = (struct vfat_dir_entry_lfn *)&vfat_dir_entry_buf;
	lfn->attr = VFAT_DIR_ENTRY_ATTR_LFN;
	lfn->zero = 0;
	lfn->csum = csum;
	lfn->cluster = 0;
	for(i = 0; i < lfn_chunks; i = i + 1)
	{
		// Prepare LFN Entry
		lfn_idx = lfn_chunks - (i + 1);
		lfn->seqnum = (i + 1) | ((lfn_idx == 0) ? VFAT_DIR_ENTRY_LFN_SEQNUM_FIRSTLAST_FLAG : 0);
		for(j = 0; j < VFAT_DIR_ENTRY_LFN_NAME_LEN_1; j = j + 1)										{ lfn->name_1[j] = (k < name_len) ? name[k] : 0; k = k + 1; }
		for(j = 0; j < VFAT_DIR_ENTRY_LFN_NAME_LEN_2; j = j + 1)										{ lfn->name_2[j] = (k < name_len) ? name[k] : 0; k = k + 1; }
		for(j = 0; j < VFAT_DIR_ENTRY_LFN_NAME_LEN_3; j = j + 1)										{ lfn->name_3[j] = (k < name_len) ? name[k] : 0; k = k + 1; }

		// Write LFN Entry
		cur_clust_idx = 0;
		cur_clust = chain_start;
		if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, idx + lfn_idx, &vfat_dir_entry_buf))			{ return 1; }
	}

	// Prepare Actual Entry
	memset(&vfat_dir_entry_buf, 0, sizeof(struct vfat_dir_entry));
	memcpy(vfat_dir_entry_buf.name, sname, VFAT_DIR_ENTRY_SNAME_LEN);
	vfat_dir_entry_buf.attr = (otype == VFS_OTYPE_DIR) ? VFAT_DIR_ENTRY_ATTR_SUBDIR : 0;
	vfat_dir_entry_buf.cluster_hi = cluster >> 16;
	vfat_dir_entry_buf.cluster_lo = cluster & 0x0000ffff;
	vfat_dir_entry_buf.fsize = (otype == VFS_OTYPE_DIR) ? 0 : size;

	// Write Entry
	cur_clust_idx = 0;
	cur_clust = chain_start;
	return vfat_dir_write(mp, &cur_clust, &cur_clust_idx, idx + lfn_chunks, &vfat_dir_entry_buf);
}

// Remove Entry from Directory
uint8_t vfat_dir_remove(struct vfs_mountpoint *mp, uint32_t chain_start, uint32_t entry_idx, uint32_t entries)
{
	uint32_t cur_clust;
	uint32_t cur_clust_idx;
	uint32_t i;

	// Run through Entries
	cur_clust_idx = 0;
	cur_clust = chain_start;
	for(i = 0; i < entries; i = i + 1)
	{
		// Read Entry
		if(vfat_dir_read(mp, &cur_clust, &cur_clust_idx, entry_idx + i, &vfat_dir_entry_buf))			{ return 1; }

		// Set Free
		vfat_dir_entry_buf.name[0] = VFAT_DIR_ENTRY_AVAIL;

		// Write Back
		if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, entry_idx + i, &vfat_dir_entry_buf))			{ return 1; }
	}

	// Are there any more deleted entries after this one?
	while(vfat_dir_entry_buf.name[0] == VFAT_DIR_ENTRY_AVAIL)
	{
		if(vfat_dir_read(mp, &cur_clust, &cur_clust_idx, entry_idx + entries, &vfat_dir_entry_buf))		{ return 1; }
		if(vfat_dir_entry_buf.name[0] == VFAT_DIR_ENTRY_AVAIL)											{ entries = entries + 1; }
	}

	// Did we reach the end?
	if(vfat_dir_entry_buf.name[0] == VFAT_DIR_ENTRY_NONE)
	{
		// We reached the end of the directory - let's see if there are any deleted entries before this one
		vfat_dir_entry_buf.name[0] = VFAT_DIR_ENTRY_AVAIL;
		while((entry_idx > 0) && (vfat_dir_entry_buf.name[0] == VFAT_DIR_ENTRY_AVAIL))
		{
			cur_clust_idx = 0;
			cur_clust = chain_start;
			if(vfat_dir_read(mp, &cur_clust, &cur_clust_idx, entry_idx - 1, &vfat_dir_entry_buf))		{ return 1; }
			if(vfat_dir_entry_buf.name[0] == VFAT_DIR_ENTRY_AVAIL)										{ entry_idx = entry_idx - 1; entries = entries + 1; }
		}

		// We can drop all of these entries - move end marker
		cur_clust_idx = 0;
		cur_clust = chain_start;
		memset(&vfat_dir_entry_buf, 0, sizeof(struct vfat_dir_entry));
		for(i = 0; i < entries; i = i + 1)																{ if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, entry_idx + i, &vfat_dir_entry_buf)) { return 1; } }
	}

	return 0;
}

// Read Directory Entry
uint8_t vfat_dir_read(struct vfs_mountpoint *mp, uint32_t *current_cluster, uint32_t *current_cluster_idx, uint32_t entry_idx, struct vfat_dir_entry *e)
{
	struct vfat_mountpoint_data *d;
	uint32_t cidx;
	uint32_t v;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Determine Cluster Index in Chain for Entry Offset
	cidx = vfat_dir_entry_cluster_idx(d, entry_idx);

	// Resolve until Cluster containing Entry
	if(*current_cluster_idx != cidx)
	{
		if(vfat_cluster_get(mp, *current_cluster, cidx - *current_cluster_idx, &v))						{ return 1; }
		*current_cluster = v; *current_cluster_idx = cidx;
	}

	// Read Cluster Data
	return vfat_cluster_read(mp, d, *current_cluster, vfat_dir_entry_offset(entry_idx) % d->cluster_size, e, sizeof(struct vfat_dir_entry));
}

// Write Directory Entry
uint8_t vfat_dir_write(struct vfs_mountpoint *mp, uint32_t *current_cluster, uint32_t *current_cluster_idx, uint32_t entry_idx, struct vfat_dir_entry *e)
{
	struct vfat_mountpoint_data *d;
	uint32_t cidx;
	uint32_t v;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Determine Cluster Index in Chain for Entry Offset
	cidx = vfat_dir_entry_cluster_idx(d, entry_idx);

	// Resolve until Cluster containing Entry
	if(*current_cluster_idx != cidx)
	{
		if(vfat_cluster_get(mp, *current_cluster, cidx - *current_cluster_idx, &v))						{ return 1; }
		*current_cluster = v; *current_cluster_idx = cidx;
	}

	// Write Cluster Data
	return vfat_cluster_write(mp, d, *current_cluster, vfat_dir_entry_offset(entry_idx) % d->cluster_size, e, sizeof(struct vfat_dir_entry));
}

// Allocate Directory Entries
uint8_t vfat_dir_alloc_entries(struct vfs_mountpoint *mp, uint32_t chain_start, uint8_t entries, uint32_t *idx)
{
	struct vfat_mountpoint_data *d;
	uint32_t e_idx;
	uint32_t free_section_start;
	uint8_t free_section_len;
	uint32_t cur_clust;
	uint32_t cur_clust_idx;
	uint32_t new_end;
	uint8_t move_end;
	uint8_t done;

	// Acquire Mountpoint Data
	d = (struct vfat_mountpoint_data *)(mp->data);

	// Run through Directory Entries
	done = 0;
	e_idx = 0;
	move_end = 0;
	cur_clust_idx = 0;
	cur_clust = chain_start;
	free_section_start = 0xffffffff;
	while(!done)
	{
		// Read Next Entry
		if(vfat_dir_read(mp, &cur_clust, &cur_clust_idx, e_idx, &vfat_dir_entry_buf))					{ return 1; }

		// Check End Of Directory / Available
		if(vfat_dir_entry_buf.name[0] == VFAT_DIR_ENTRY_NONE)
		{
			if(free_section_start == 0xffffffff)														{ free_section_start = e_idx; free_section_len = 0; }
			done = 1;
			move_end = 1;
		}
		else if(vfat_dir_entry_buf.name[0] == VFAT_DIR_ENTRY_AVAIL)
		{
			// Count Free Entries in Section
			if(free_section_start == 0xffffffff)														{ free_section_start = e_idx; free_section_len = 1; }
			else
			{
				// Do we already have enough free chunks?
				free_section_len = free_section_len + 1;
				if(free_section_len == entries)															{ done = 1; }
			}
		}
		else																							{ free_section_start = 0xffffffff; }

		// Next
		if(!done)																						{ e_idx = e_idx + 1; }
	}

	// Inc Entries if we need to move end
	if(move_end)
	{
		// Determine new end index
		new_end = free_section_start + (entries - (free_section_len));

		// Do we need to allocate another cluster for this directory?
		if(vfat_dir_entry_cluster_idx(d, free_section_start) != vfat_dir_entry_cluster_idx(d, free_section_start + entries))
		{
			// Attempt to allocate a new cluster
			if(vfat_cluster_chain_extend(mp, chain_start, 0))											{ return 1; }
		}

		// Set new End Marker
		cur_clust_idx = 0;
		cur_clust = chain_start;
		memset(&vfat_dir_entry_buf, 0, sizeof(struct vfat_dir_entry));
		if(vfat_dir_write(mp, &cur_clust, &cur_clust_idx, new_end, &vfat_dir_entry_buf))				{ return 1; }
	}

	// Set Index
	*idx = free_section_start;

	return 0;
}

// Allocate Short Name for Object
uint8_t vfat_dir_alloc_sname(struct vfs_mountpoint *mp, uint32_t chain_start, char *name, uint8_t name_len, uint8_t *short_name)
{
	uint8_t *sname_name_buf;
	uint8_t *sname_ext_buf;
	uint32_t cur_clust;
	uint32_t cur_clust_idx;
	uint32_t e_idx;
	uint8_t name_pre_len;
	uint8_t ext_len;
	uint8_t sname_len;
	uint8_t sname_n_len;
	uint8_t e_sname_len;
	uint8_t e_sname_n_len;
	uint16_t e_sname_idx;
	uint16_t sname_idx;
	uint8_t done;
	uint8_t has_idx;
	uint8_t idx_len;
	uint8_t i;

	// Prepare Short Name Template
	sname_name_buf = short_name;
	sname_ext_buf = &(short_name[VFAT_DIR_ENTRY_NAME_LEN]);
	sname_len = 0;
	sname_idx = 0;
	name_pre_len = str_chr_r(name, name_len, '.');
	for(i = 0; i < VFAT_DIR_ENTRY_NAME_LEN; i = i + 1)
	{
		if(i < name_pre_len)																			{ sname_name_buf[i] = upcase_c(name[i]); sname_len = i + 1; }
		else																							{ sname_name_buf[i] = ' '; }
	}
	sname_n_len = sname_len;
	if(name_pre_len > sname_len)
	{
		sname_name_buf[sname_len - 2] = '~';
		sname_name_buf[sname_len - 1] = '1';
		sname_n_len = sname_len - 2;
		sname_idx = 1;
	}

	// Prepare Extension
	ext_len = (name_len > name_pre_len) ? (name_len - (name_pre_len + 1)) : 0;
	if(ext_len > VFAT_DIR_ENTRY_EXT_LEN)																{ ext_len = VFAT_DIR_ENTRY_EXT_LEN; }
	for(i = 0; i < VFAT_DIR_ENTRY_EXT_LEN; i = i + 1)
	{
		if(i < ext_len)																					{ sname_ext_buf[i] = upcase_c(name[name_pre_len + 1 + i]); }
		else																							{ sname_ext_buf[i] = ' '; }
	}

	// Run through Directory
	done = 0;
	e_idx = 0;
	cur_clust_idx = 0;
	cur_clust = chain_start;
	while(!done)
	{
		// Read Entry
		if(vfat_dir_read(mp, &cur_clust, &cur_clust_idx, e_idx, &vfat_dir_entry_buf))					{ return 1; }

		// Check End Of Directory
		if(vfat_dir_entry_buf.name[0] == VFAT_DIR_ENTRY_NONE)											{ done = 1; }
		else
		{
			// Check Real Entry (not available / deleted / LFN) & Extension Match
			if((vfat_dir_entry_buf.name[0] != VFAT_DIR_ENTRY_AVAIL) && ((vfat_dir_entry_buf.attr & VFAT_DIR_ENTRY_ATTR_LFN) != VFAT_DIR_ENTRY_ATTR_LFN) && (memcmp(sname_ext_buf, vfat_dir_entry_buf.ext, VFAT_DIR_ENTRY_EXT_LEN) == 0))
			{
				// Determine Entry's Short Name Length
				e_sname_len = VFAT_DIR_ENTRY_NAME_LEN;
				if(vfat_dir_entry_buf.name[e_sname_len - 1] == ' ')										{ e_sname_len = e_sname_len - 1; while((vfat_dir_entry_buf.name[e_sname_len] == ' ') && (e_sname_len > 0)) { e_sname_len = e_sname_len - 1; } }

				// Determine Actual "Name" Length (Start of Name Index)
				e_sname_n_len = str_chr_r((char *)(vfat_dir_entry_buf.name), e_sname_len, '~');

				// Verify Name Match
				if(e_sname_n_len <= sname_n_len)
				{
					// Check Name
					if(memcmp(vfat_dir_entry_buf.name, sname_name_buf, e_sname_n_len) == 0)
					{
						// Match - Check if entry has an index
						has_idx = 0;
						if(e_sname_n_len < e_sname_len)
						{
							// Check it's really an Index
							has_idx = 1;
							for(i = e_sname_n_len + 1; i < (e_sname_len); i = i + 1)					{ if((vfat_dir_entry_buf.name[i] < '0') || (vfat_dir_entry_buf.name[i] > '9')) { has_idx = 0; } }
							if(has_idx)
							{
								// Get Name Index
								memcpy(vfat_dir_sname_idx_buf, &(vfat_dir_entry_buf.name[e_sname_n_len + 1]), e_sname_len - (e_sname_n_len + 1));
								vfat_dir_sname_idx_buf[e_sname_len - (e_sname_n_len + 1)] = 0;
								e_sname_idx = atoi((char *)vfat_dir_sname_idx_buf);
							}
						}

						// Does entry have an Index? { Inc Index }
						if((has_idx) && (e_sname_idx >= sname_idx))										{ sname_idx = e_sname_idx + 1; }
					}
				}
			}
		}

		// Next
		if(!done)																						{ e_idx = e_idx + 1; }
	}

	// Check Index
	if(sname_idx)
	{
		// Limit Name Index
		if(sname_idx > VFAT_DIR_ENTRY_SNAME_IDX_MAX)													{ return 1; }

		// Update Short Name
		idx_len = csnprintf(vfat_dir_sname_idx_buf, VFAT_DIR_ENTRY_NAME_LEN, "%i", sname_idx);
		sname_n_len = sname_len;
		if((sname_n_len + idx_len + 1) > VFAT_DIR_ENTRY_NAME_LEN)										{ sname_n_len = VFAT_DIR_ENTRY_NAME_LEN - (idx_len + 1); }
		sname_name_buf[sname_n_len] = '~';
		memcpy(&(sname_name_buf[sname_n_len + 1]), vfat_dir_sname_idx_buf, idx_len);
		sname_len = sname_n_len + idx_len + 1;
	}

	return 0;
}

// Compute Checksum for Short Name
uint8_t vfat_dir_short_name_csum(uint8_t *short_name)
{
	uint8_t res;
	uint8_t i;

	// Run through Text { Compute Checksum }
	res = 0;
	for(i = 0; i < VFAT_DIR_ENTRY_SNAME_LEN; i = i + 1)													{ res = ((res & 0x01) << 7) + (res >> 1) + short_name[i]; }

	return res;
}

// Extract Short Name from Directory Entry
void vfat_dir_entry_get_short_name(uint8_t *short_name, char *buf, uint8_t *len)
{
	uint8_t l;
	uint8_t el;
	uint8_t *ext;

	// Determine Length for Name Part
	l = VFAT_DIR_ENTRY_NAME_LEN;
	while((l > 0) && (short_name[l - 1] == ' '))														{ l = l - 1; }

	// Copy Name Part & Add dot
	memcpy(buf, short_name, l);

	// Determine Length for Extension Part
	el = VFAT_DIR_ENTRY_EXT_LEN;
	ext = &(short_name[VFAT_DIR_ENTRY_NAME_LEN]);
	while((el > 0) && (ext[el - 1] == ' '))																{ el = el - 1; }

	// Copy Extension
	if(el)
	{
		buf[l] = '.';
		l = l + 1;
		memcpy(&(buf[l]), ext, el);
		l = l + el;
	}

	// Set Length
	*len = l;
}
